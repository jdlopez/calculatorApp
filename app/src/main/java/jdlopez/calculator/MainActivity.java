package jdlopez.calculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Stack;

public class MainActivity extends AppCompatActivity {
    private boolean ERROR = false;

    private String equation = "";
    private int numbers[] = {R.id.one, R.id.two, R.id.three, R.id.four, R.id.five, R.id.six, R.id.seven, R.id.eight, R.id.nine, R.id.zero, R.id.decimal};
    private int operators[] = {R.id.add, R.id.sub, R.id.multiply, R.id.divide, R.id.leftP, R.id.rightP};
    private TextView screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.screen = (TextView) findViewById(R.id.resultTextView);

        setNumbersListener();
        setOperationsListener();
    }
    private void setNumbersListener(){
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ERROR) {
                    Button btn = (Button) v;
                    screen.append(btn.getText());
                }
            }
        };
        for(int id: numbers)
            findViewById(id).setOnClickListener(listener);
    }

    private void setOperationsListener(){
        View.OnClickListener listener = new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(!ERROR) {
                    Button btn = (Button) v;
                    screen.append(" " + btn.getText() + " ");
                }
            }
        };

        for(int id: operators)
            findViewById(id).setOnClickListener(listener);

        findViewById(R.id.clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clear();
            }
        });

        findViewById(R.id.equals).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onEqual();
            }
        });

        findViewById(R.id.delete).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                screen.setText(delete(screen.getText().toString()));
            }
        });
    }

    private void clear(){
        screen.setText("");
        ERROR = false;
    }
    private void onEqual(){
        // Read the expression
        String txt = screen.getText().toString();
        // Create an Expression (A class from exp4j library)
        try {
            // Calculate the result and display
            if(ERROR)
                screen.setText("Error");
            else {
                if(!txt.equals("")) {
                    double ans = evaluate(txt);
                    screen.setText(" " + (format(ans)));
                }
            }
        } catch (ArithmeticException ex) {
                screen.setText("Error");
            // Display an error message
        }
    }

    private String delete(String txt){
        String result = "";

        result = txt.substring(0, txt.length()-1);

        return result;
    }

    private String format(double ans){
        char[] answer = Double.toString(ans).toCharArray();
        String result = "";
        int i = 0;
        if(answer[i] == '-') {
            result = "- ";
            i++;
        }
        for(;i < answer.length; i++){
            result += answer[i];
        }

        return result;
    }

    private double evaluate(String expression)
    {
        char[] tokens = expression.toCharArray();

        // Stack for numbers: 'values'
        Stack<Double> values = new Stack<Double>();

        // Stack for Operators: 'ops'
        Stack<Character> ops = new Stack<Character>();

        for (int i = 0; i < tokens.length; i++)
        {
            // Current token is a whitespace, skip it
            if (tokens[i] == ' ')
                continue;
            boolean neg = false;

            if(tokens[i] == '-'){
                if(i == 1 || (i > 3 && isOp(tokens[i-3])))
                    neg = true;
            }
            // Current token is a number, push it to stack for numbers
            if (tokens[i] >= '0' && tokens[i] <= '9' || tokens[i] == '.' || neg)
            {
                StringBuffer sbuf = new StringBuffer();
                // There may be more than one digits in number
                while (i < tokens.length && ((tokens[i] >= '0' && tokens[i] <= '9') || tokens[i] == '.' || tokens[i] == '-')){
                    sbuf.append(tokens[i++]);
                    if(neg){
                        neg = false;
                        i++;
                    }
                }
                values.push(Double.parseDouble(sbuf.toString()));
            }

            // Current token is an opening brace, push it to 'ops'
            else if (tokens[i] == '(')
                ops.push(tokens[i]);

                // Closing brace encountered, solve entire brace
            else if (tokens[i] == ')')
            {
                while (ops.peek() != '(')
                    values.push(applyOp(ops.pop(), values.pop(), values.pop()));
                ops.pop();
            }

            // Current token is an operator.
            else if(i < tokens.length && (tokens[i] == '+' || tokens[i] == '-' ||
                    tokens[i] == '*' || tokens[i] == '/'))
            {
                // While top of 'ops' has same or greater precedence to current
                // token, which is an operator. Apply operator on top of 'ops'
                // to top two elements in values stack
                while (!ops.empty() && hasPrecedence(tokens[i], ops.peek()))
                    values.push(applyOp(ops.pop(), values.pop(), values.pop()));

                // Push current token to 'ops'.
                ops.push(tokens[i]);
            }
        }

        // Entire expression has been parsed at this point, apply remaining
        // ops to remaining values
        while (!ops.empty())
            values.push(applyOp(ops.pop(), values.pop(), values.pop()));

        // Top of 'values' contains result, return it
        return values.pop();
    }

    // Returns true if 'op2' has higher or same precedence as 'op1',
    // otherwise returns false.
    private boolean hasPrecedence(char op1, char op2)
    {
        if (op2 == '(' || op2 == ')')
            return false;
        if ((op1 == '*' || op1 == '/') && (op2 == '+' || op2 == '-'))
            return false;
        else
            return true;
    }

    // A utility method to apply an operator 'op' on operands 'a'
    // and 'b'. Return the result.
    private double applyOp(char op, double b, double a)
    {
        switch (op)
        {
            case '+':
                return a + b;
            case '-':
                return a - b;
            case '*':
                return a * b;
            case '/':
                if (b == 0) {
                    //throw new UnsupportedOperationException("Cannot divide by zero");
                    ERROR = true;
                }
                return a / b;
        }
        return 0;
    }

    private boolean isOp(char token){
        if(token == '+' || token == '-' || token == '*' || token == '/')
            return true;
        return false;
    }
}
